import { PreloadingStrategy, Route } from "@angular/router";
import { Observable } from "rxjs";
import * as i0 from "@angular/core";
export declare class WithDataCustomPreloadStrategy implements PreloadingStrategy {
    preload(route: Route, fn: () => Observable<any>): Observable<any>;
    static ɵfac: i0.ɵɵFactoryDeclaration<WithDataCustomPreloadStrategy, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<WithDataCustomPreloadStrategy>;
}
//# sourceMappingURL=with-data-custom-preload-strategy.d.ts.map