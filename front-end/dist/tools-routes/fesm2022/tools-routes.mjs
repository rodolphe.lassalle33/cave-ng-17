import * as i0 from '@angular/core';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';

class WithDataCustomPreloadStrategy {
    preload(route, fn) {
        let result = of(null);
        if (route.data?.['preload']) {
            result = fn();
        }
        return result;
    }
    static { this.ɵfac = function WithDataCustomPreloadStrategy_Factory(t) { return new (t || WithDataCustomPreloadStrategy)(); }; }
    static { this.ɵprov = /*@__PURE__*/ i0.ɵɵdefineInjectable({ token: WithDataCustomPreloadStrategy, factory: WithDataCustomPreloadStrategy.ɵfac }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(WithDataCustomPreloadStrategy, [{
        type: Injectable
    }], null, null); })();

/*
 * Public API Surface of tools-routes
 */

/**
 * Generated bundle index. Do not edit.
 */

export { WithDataCustomPreloadStrategy };
//# sourceMappingURL=tools-routes.mjs.map
