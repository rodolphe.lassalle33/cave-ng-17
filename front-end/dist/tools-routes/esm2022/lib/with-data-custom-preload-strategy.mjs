import { Injectable } from "@angular/core";
import { of } from "rxjs";
import * as i0 from "@angular/core";
export class WithDataCustomPreloadStrategy {
    preload(route, fn) {
        let result = of(null);
        if (route.data?.['preload']) {
            result = fn();
        }
        return result;
    }
    static { this.ɵfac = function WithDataCustomPreloadStrategy_Factory(t) { return new (t || WithDataCustomPreloadStrategy)(); }; }
    static { this.ɵprov = /*@__PURE__*/ i0.ɵɵdefineInjectable({ token: WithDataCustomPreloadStrategy, factory: WithDataCustomPreloadStrategy.ɵfac }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(WithDataCustomPreloadStrategy, [{
        type: Injectable
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2l0aC1kYXRhLWN1c3RvbS1wcmVsb2FkLXN0cmF0ZWd5LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vcHJvamVjdHMvdG9vbHMtcm91dGVzL3NyYy9saWIvd2l0aC1kYXRhLWN1c3RvbS1wcmVsb2FkLXN0cmF0ZWd5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFM0MsT0FBTyxFQUFjLEVBQUUsRUFBRSxNQUFNLE1BQU0sQ0FBQzs7QUFHdEMsTUFBTSxPQUFPLDZCQUE2QjtJQUV4QyxPQUFPLENBQUMsS0FBWSxFQUFFLEVBQXlCO1FBQzdDLElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUV0QixJQUFHLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQyxTQUFTLENBQUMsRUFBRTtZQUMxQixNQUFNLEdBQUcsRUFBRSxFQUFFLENBQUM7U0FDZjtRQUVELE9BQU8sTUFBTSxDQUFDO0lBQ2hCLENBQUM7OEZBVlUsNkJBQTZCO3VFQUE3Qiw2QkFBNkIsV0FBN0IsNkJBQTZCOztpRkFBN0IsNkJBQTZCO2NBRHpDLFVBQVUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IFByZWxvYWRpbmdTdHJhdGVneSwgUm91dGUgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBvZiB9IGZyb20gXCJyeGpzXCI7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBXaXRoRGF0YUN1c3RvbVByZWxvYWRTdHJhdGVneSBpbXBsZW1lbnRzIFByZWxvYWRpbmdTdHJhdGVneSB7XG5cbiAgcHJlbG9hZChyb3V0ZTogUm91dGUsIGZuOiAoKSA9PiBPYnNlcnZhYmxlPGFueT4pOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIGxldCByZXN1bHQgPSBvZihudWxsKTtcblxuICAgIGlmKHJvdXRlLmRhdGE/LlsncHJlbG9hZCddKSB7XG4gICAgICByZXN1bHQgPSBmbigpOyBcbiAgICB9XG5cbiAgICByZXR1cm4gcmVzdWx0O1xuICB9XG5cbn0iXX0=