import * as i0 from '@angular/core';
import { Injectable, inject, Component } from '@angular/core';
import { delay, of, shareReplay } from 'rxjs';
import * as i1 from '@angular/common';
import { CommonModule } from '@angular/common';

const inMemoryGetAllBeers = {
    getAll: () => {
        const table = [
            {
                _id: '1',
                name: 'Leffe',
                type: 'bof',
                country: {
                    _id: '1',
                    name: 'france',
                },
            },
        ];
        return of(table).pipe(delay(1500));
    },
};
const mockingFactory = () => inMemoryGetAllBeers;
class GetAllBeersInfrastructure {
    getAll() {
        throw new Error('Method not implemented.');
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.0.8", ngImport: i0, type: GetAllBeersInfrastructure, deps: [], target: i0.ɵɵFactoryTarget.Injectable }); }
    static { this.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "17.0.8", ngImport: i0, type: GetAllBeersInfrastructure, providedIn: 'root', useFactory: mockingFactory }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.0.8", ngImport: i0, type: GetAllBeersInfrastructure, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root',
                    useFactory: mockingFactory,
                }]
        }] });

class GetAllBeersApplication {
    constructor() {
        this.api = inject(GetAllBeersInfrastructure);
        this.getAllBeers$ = this.api.getAll().pipe(shareReplay(1));
    }
    getAll() {
        return this.getAllBeers$;
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.0.8", ngImport: i0, type: GetAllBeersApplication, deps: [], target: i0.ɵɵFactoryTarget.Injectable }); }
    static { this.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "17.0.8", ngImport: i0, type: GetAllBeersApplication, providedIn: 'root' }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.0.8", ngImport: i0, type: GetAllBeersApplication, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }] });

class ListBeersComponent {
    constructor() {
        this.beers$ = inject(GetAllBeersApplication).getAll();
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.0.8", ngImport: i0, type: ListBeersComponent, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "17.0.0", version: "17.0.8", type: ListBeersComponent, isStandalone: true, selector: "dtbc-list-beers", ngImport: i0, template: "@for (beer of beers$ |async; track beer._id) {\n  <div>\n    {{beer.name}}\n  </div>\n}\n", styles: [""], dependencies: [{ kind: "ngmodule", type: CommonModule }, { kind: "pipe", type: i1.AsyncPipe, name: "async" }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.0.8", ngImport: i0, type: ListBeersComponent, decorators: [{
            type: Component,
            args: [{ selector: 'dtbc-list-beers', standalone: true, imports: [CommonModule], template: "@for (beer of beers$ |async; track beer._id) {\n  <div>\n    {{beer.name}}\n  </div>\n}\n" }]
        }] });

class PageListBeersComponent {
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.0.8", ngImport: i0, type: PageListBeersComponent, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.0.8", type: PageListBeersComponent, isStandalone: true, selector: "dtbc-page-list-beers", ngImport: i0, template: "<dtbc-list-beers></dtbc-list-beers>\n", styles: [""], dependencies: [{ kind: "ngmodule", type: CommonModule }, { kind: "component", type: ListBeersComponent, selector: "dtbc-list-beers" }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.0.8", ngImport: i0, type: PageListBeersComponent, decorators: [{
            type: Component,
            args: [{ selector: 'dtbc-page-list-beers', standalone: true, imports: [CommonModule, ListBeersComponent], template: "<dtbc-list-beers></dtbc-list-beers>\n" }]
        }] });

const caveRoutes = [
    {
        path: '',
        component: PageListBeersComponent
    }
];

/*
 * Public API Surface of cave-feature
 */
// SERVICE

/**
 * Generated bundle index. Do not edit.
 */

export { GetAllBeersApplication, ListBeersComponent, PageListBeersComponent, caveRoutes };
//# sourceMappingURL=cave-feature.mjs.map
