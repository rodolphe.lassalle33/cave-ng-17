import { Component, inject } from '@angular/core';
import { GetAllBeersApplication } from '../../services/beers.application';
import { CommonModule } from '@angular/common';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
export class ListBeersComponent {
    constructor() {
        this.beers$ = inject(GetAllBeersApplication).getAll();
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.0.8", ngImport: i0, type: ListBeersComponent, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "17.0.0", version: "17.0.8", type: ListBeersComponent, isStandalone: true, selector: "dtbc-list-beers", ngImport: i0, template: "@for (beer of beers$ |async; track beer._id) {\n  <div>\n    {{beer.name}}\n  </div>\n}\n", styles: [""], dependencies: [{ kind: "ngmodule", type: CommonModule }, { kind: "pipe", type: i1.AsyncPipe, name: "async" }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.0.8", ngImport: i0, type: ListBeersComponent, decorators: [{
            type: Component,
            args: [{ selector: 'dtbc-list-beers', standalone: true, imports: [CommonModule], template: "@for (beer of beers$ |async; track beer._id) {\n  <div>\n    {{beer.name}}\n  </div>\n}\n" }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGlzdC1iZWVycy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9jYXZlLWZlYXR1cmUvc3JjL2xpYi9jb21wb25lbnRzL2xpc3QtYmVlcnMvbGlzdC1iZWVycy5jb21wb25lbnQudHMiLCIuLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9jYXZlLWZlYXR1cmUvc3JjL2xpYi9jb21wb25lbnRzL2xpc3QtYmVlcnMvbGlzdC1iZWVycy5jb21wb25lbnQuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNsRCxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUMxRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7OztBQVMvQyxNQUFNLE9BQU8sa0JBQWtCO0lBUC9CO1FBUUUsV0FBTSxHQUFHLE1BQU0sQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO0tBQ2xEOzhHQUZZLGtCQUFrQjtrR0FBbEIsa0JBQWtCLDJFQ1gvQiwyRkFLQSx5RERFWSxZQUFZOzsyRkFJWCxrQkFBa0I7a0JBUDlCLFNBQVM7K0JBQ0UsaUJBQWlCLGNBQ2YsSUFBSSxXQUNQLENBQUMsWUFBWSxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBpbmplY3QgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEdldEFsbEJlZXJzQXBwbGljYXRpb24gfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9iZWVycy5hcHBsaWNhdGlvbic7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkdGJjLWxpc3QtYmVlcnMnLFxuICBzdGFuZGFsb25lOiB0cnVlLFxuICBpbXBvcnRzOiBbQ29tbW9uTW9kdWxlXSxcbiAgdGVtcGxhdGVVcmw6ICcuL2xpc3QtYmVlcnMuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybDogJy4vbGlzdC1iZWVycy5jb21wb25lbnQuY3NzJ1xufSlcbmV4cG9ydCBjbGFzcyBMaXN0QmVlcnNDb21wb25lbnQge1xuICBiZWVycyQgPSBpbmplY3QoR2V0QWxsQmVlcnNBcHBsaWNhdGlvbikuZ2V0QWxsKCk7XG59XG4iLCJAZm9yIChiZWVyIG9mIGJlZXJzJCB8YXN5bmM7IHRyYWNrIGJlZXIuX2lkKSB7XG4gIDxkaXY+XG4gICAge3tiZWVyLm5hbWV9fVxuICA8L2Rpdj5cbn1cbiJdfQ==