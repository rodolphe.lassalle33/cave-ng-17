import { delay, of } from 'rxjs';
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
export const inMemoryGetAllBeers = {
    getAll: () => {
        const table = [
            {
                _id: '1',
                name: 'Leffe',
                type: 'bof',
                country: {
                    _id: '1',
                    name: 'france',
                },
            },
        ];
        return of(table).pipe(delay(1500));
    },
};
export const mockingFactory = () => inMemoryGetAllBeers;
export class GetAllBeersInfrastructure {
    getAll() {
        throw new Error('Method not implemented.');
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.0.8", ngImport: i0, type: GetAllBeersInfrastructure, deps: [], target: i0.ɵɵFactoryTarget.Injectable }); }
    static { this.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "17.0.8", ngImport: i0, type: GetAllBeersInfrastructure, providedIn: 'root', useFactory: mockingFactory }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.0.8", ngImport: i0, type: GetAllBeersInfrastructure, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root',
                    useFactory: mockingFactory,
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmVlcnMuaW5mcmFzdHJ1Y3R1cmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9jYXZlLWZlYXR1cmUvc3JjL2xpYi9zZXJ2aWNlcy9iZWVycy5pbmZyYXN0cnVjdHVyZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQWMsS0FBSyxFQUFFLEVBQUUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUU3QyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDOztBQU0zQyxNQUFNLENBQUMsTUFBTSxtQkFBbUIsR0FBZ0I7SUFDOUMsTUFBTSxFQUFFLEdBQUcsRUFBRTtRQUNYLE1BQU0sS0FBSyxHQUFVO1lBQ25CO2dCQUNFLEdBQUcsRUFBRSxHQUFHO2dCQUNSLElBQUksRUFBRSxPQUFPO2dCQUNiLElBQUksRUFBRSxLQUFLO2dCQUNYLE9BQU8sRUFBRTtvQkFDUCxHQUFHLEVBQUUsR0FBRztvQkFDUixJQUFJLEVBQUUsUUFBUTtpQkFDZjthQUNGO1NBQ0YsQ0FBQztRQUNGLE9BQU8sRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztJQUNyQyxDQUFDO0NBQ0YsQ0FBQztBQUVGLE1BQU0sQ0FBQyxNQUFNLGNBQWMsR0FBRyxHQUFHLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQztBQU14RCxNQUFNLE9BQU8seUJBQXlCO0lBQ3BDLE1BQU07UUFDSixNQUFNLElBQUksS0FBSyxDQUFDLHlCQUF5QixDQUFDLENBQUM7SUFDN0MsQ0FBQzs4R0FIVSx5QkFBeUI7a0hBQXpCLHlCQUF5QixjQUh4QixNQUFNLGNBQ04sY0FBYzs7MkZBRWYseUJBQXlCO2tCQUpyQyxVQUFVO21CQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO29CQUNsQixVQUFVLEVBQUUsY0FBYztpQkFDM0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBPYnNlcnZhYmxlLCBkZWxheSwgb2YgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IEJlZXJzIH0gZnJvbSAnLi4vbW9kZWxzL2JlZXJzJztcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuZXhwb3J0IGludGVyZmFjZSBHZXRBbGxCZWVycyB7XG4gIGdldEFsbCgpOiBPYnNlcnZhYmxlPEJlZXJzPjtcbn1cblxuZXhwb3J0IGNvbnN0IGluTWVtb3J5R2V0QWxsQmVlcnM6IEdldEFsbEJlZXJzID0ge1xuICBnZXRBbGw6ICgpID0+IHtcbiAgICBjb25zdCB0YWJsZTogQmVlcnMgPSBbXG4gICAgICB7XG4gICAgICAgIF9pZDogJzEnLFxuICAgICAgICBuYW1lOiAnTGVmZmUnLFxuICAgICAgICB0eXBlOiAnYm9mJyxcbiAgICAgICAgY291bnRyeToge1xuICAgICAgICAgIF9pZDogJzEnLFxuICAgICAgICAgIG5hbWU6ICdmcmFuY2UnLFxuICAgICAgICB9LFxuICAgICAgfSxcbiAgICBdO1xuICAgIHJldHVybiBvZih0YWJsZSkucGlwZShkZWxheSgxNTAwKSk7XG4gIH0sXG59O1xuXG5leHBvcnQgY29uc3QgbW9ja2luZ0ZhY3RvcnkgPSAoKSA9PiBpbk1lbW9yeUdldEFsbEJlZXJzO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290JyxcbiAgdXNlRmFjdG9yeTogbW9ja2luZ0ZhY3RvcnksXG59KVxuZXhwb3J0IGNsYXNzIEdldEFsbEJlZXJzSW5mcmFzdHJ1Y3R1cmUgaW1wbGVtZW50cyBHZXRBbGxCZWVycyB7XG4gIGdldEFsbCgpOiBPYnNlcnZhYmxlPEJlZXJzPiB7XG4gICAgdGhyb3cgbmV3IEVycm9yKCdNZXRob2Qgbm90IGltcGxlbWVudGVkLicpO1xuICB9XG59XG4iXX0=