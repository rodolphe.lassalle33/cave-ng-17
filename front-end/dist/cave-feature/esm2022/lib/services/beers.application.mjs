import { Injectable, inject } from "@angular/core";
import { shareReplay } from "rxjs";
import { GetAllBeersInfrastructure } from "./beers.infrastructure";
import * as i0 from "@angular/core";
export class GetAllBeersApplication {
    constructor() {
        this.api = inject(GetAllBeersInfrastructure);
        this.getAllBeers$ = this.api.getAll().pipe(shareReplay(1));
    }
    getAll() {
        return this.getAllBeers$;
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.0.8", ngImport: i0, type: GetAllBeersApplication, deps: [], target: i0.ɵɵFactoryTarget.Injectable }); }
    static { this.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "17.0.8", ngImport: i0, type: GetAllBeersApplication, providedIn: 'root' }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.0.8", ngImport: i0, type: GetAllBeersApplication, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmVlcnMuYXBwbGljYXRpb24uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9jYXZlLWZlYXR1cmUvc3JjL2xpYi9zZXJ2aWNlcy9iZWVycy5hcHBsaWNhdGlvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNuRCxPQUFPLEVBQWMsV0FBVyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBRS9DLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLHdCQUF3QixDQUFDOztBQUtuRSxNQUFNLE9BQU8sc0JBQXNCO0lBSG5DO1FBSW1CLFFBQUcsR0FBRyxNQUFNLENBQUMseUJBQXlCLENBQUMsQ0FBQztRQUN4QyxpQkFBWSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0tBS3hFO0lBSEMsTUFBTTtRQUNKLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQztJQUMzQixDQUFDOzhHQU5VLHNCQUFzQjtrSEFBdEIsc0JBQXNCLGNBRnJCLE1BQU07OzJGQUVQLHNCQUFzQjtrQkFIbEMsVUFBVTttQkFBQztvQkFDVixVQUFVLEVBQUUsTUFBTTtpQkFDbkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBpbmplY3QgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgc2hhcmVSZXBsYXkgfSBmcm9tIFwicnhqc1wiO1xuaW1wb3J0IHsgQmVlcnMgfSBmcm9tIFwiLi4vbW9kZWxzL2JlZXJzXCI7XG5pbXBvcnQgeyBHZXRBbGxCZWVyc0luZnJhc3RydWN0dXJlIH0gZnJvbSBcIi4vYmVlcnMuaW5mcmFzdHJ1Y3R1cmVcIjtcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgR2V0QWxsQmVlcnNBcHBsaWNhdGlvbiB7XG4gIHByaXZhdGUgcmVhZG9ubHkgYXBpID0gaW5qZWN0KEdldEFsbEJlZXJzSW5mcmFzdHJ1Y3R1cmUpO1xuICBwcml2YXRlIHJlYWRvbmx5IGdldEFsbEJlZXJzJCA9IHRoaXMuYXBpLmdldEFsbCgpLnBpcGUoc2hhcmVSZXBsYXkoMSkpO1xuXG4gIGdldEFsbCgpOiBPYnNlcnZhYmxlPEJlZXJzPiB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0QWxsQmVlcnMkO1xuICB9XG59XG4iXX0=