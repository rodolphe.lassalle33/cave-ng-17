import { Observable } from "rxjs";
import { Beers } from "../models/beers";
import * as i0 from "@angular/core";
export declare class GetAllBeersApplication {
    private readonly api;
    private readonly getAllBeers$;
    getAll(): Observable<Beers>;
    static ɵfac: i0.ɵɵFactoryDeclaration<GetAllBeersApplication, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<GetAllBeersApplication>;
}
