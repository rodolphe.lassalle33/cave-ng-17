import { Observable } from 'rxjs';
import { Beers } from '../models/beers';
import * as i0 from "@angular/core";
export interface GetAllBeers {
    getAll(): Observable<Beers>;
}
export declare const inMemoryGetAllBeers: GetAllBeers;
export declare const mockingFactory: () => GetAllBeers;
export declare class GetAllBeersInfrastructure implements GetAllBeers {
    getAll(): Observable<Beers>;
    static ɵfac: i0.ɵɵFactoryDeclaration<GetAllBeersInfrastructure, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<GetAllBeersInfrastructure>;
}
