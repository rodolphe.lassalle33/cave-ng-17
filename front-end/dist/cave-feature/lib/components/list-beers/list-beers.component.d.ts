import * as i0 from "@angular/core";
export declare class ListBeersComponent {
    beers$: import("rxjs").Observable<import("../../models/beers").Beers>;
    static ɵfac: i0.ɵɵFactoryDeclaration<ListBeersComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<ListBeersComponent, "dtbc-list-beers", never, {}, {}, never, never, true, never>;
}
