import { Routes } from "@angular/router";
import { PageListBeersComponent } from "./pages/page-list-beers/page-list-beers.component";

export const caveRoutes: Routes = [
  {
    path: '',
    component: PageListBeersComponent
  }
];