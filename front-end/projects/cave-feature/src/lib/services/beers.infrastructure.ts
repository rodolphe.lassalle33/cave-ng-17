import { Observable, delay, of } from 'rxjs';
import { Beers } from '../models/beers';
import { Injectable } from '@angular/core';

export interface GetAllBeers {
  getAll(): Observable<Beers>;
}

export const inMemoryGetAllBeers: GetAllBeers = {
  getAll: () => {
    const table: Beers = [
      {
        _id: '1',
        name: 'Leffe',
        type: 'bof',
        country: {
          _id: '1',
          name: 'france',
        },
      },
    ];
    return of(table).pipe(delay(1500));
  },
};

export const mockingFactory = () => inMemoryGetAllBeers;

@Injectable({
  providedIn: 'root',
  useFactory: mockingFactory,
})
export class GetAllBeersInfrastructure implements GetAllBeers {
  getAll(): Observable<Beers> {
    throw new Error('Method not implemented.');
  }
}
