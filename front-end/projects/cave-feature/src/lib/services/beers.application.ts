import { Injectable, inject } from "@angular/core";
import { Observable, shareReplay } from "rxjs";
import { Beers } from "../models/beers";
import { GetAllBeersInfrastructure } from "./beers.infrastructure";

@Injectable({
  providedIn: 'root'
})
export class GetAllBeersApplication {
  private readonly api = inject(GetAllBeersInfrastructure);
  private readonly getAllBeers$ = this.api.getAll().pipe(shareReplay(1));

  getAll(): Observable<Beers> {
    return this.getAllBeers$;
  }
}
