import { Country } from "./country";

export interface Beer {
  _id: string;
  name: string;
  type: string;
  country: Country;
}

export type Beers = Beer[];