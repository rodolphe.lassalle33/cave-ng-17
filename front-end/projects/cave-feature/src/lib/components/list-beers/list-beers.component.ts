import { Component, inject } from '@angular/core';
import { GetAllBeersApplication } from '../../services/beers.application';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'dtbc-list-beers',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './list-beers.component.html',
  styleUrl: './list-beers.component.css'
})
export class ListBeersComponent {
  beers$ = inject(GetAllBeersApplication).getAll();
}
