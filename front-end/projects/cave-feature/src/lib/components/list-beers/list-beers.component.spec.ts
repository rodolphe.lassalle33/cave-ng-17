import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListBeersComponent } from './list-beers.component';

describe('ListBeersComponent', () => {
  let component: ListBeersComponent;
  let fixture: ComponentFixture<ListBeersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ListBeersComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ListBeersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
