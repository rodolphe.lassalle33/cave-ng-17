import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { ListBeersComponent } from '../../../public-api';

@Component({
  selector: 'dtbc-page-list-beers',
  standalone: true,
  imports: [CommonModule, ListBeersComponent],
  templateUrl: './page-list-beers.component.html',
  styleUrl: './page-list-beers.component.css'
})
export class PageListBeersComponent {

}
