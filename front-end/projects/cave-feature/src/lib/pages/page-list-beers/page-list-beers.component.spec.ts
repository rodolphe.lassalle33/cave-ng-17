import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageListBeersComponent } from './page-list-beers.component';

describe('PageListBeersComponent', () => {
  let component: PageListBeersComponent;
  let fixture: ComponentFixture<PageListBeersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PageListBeersComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PageListBeersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
