import { Routes } from '@angular/router';

export const routes: Routes = [{
  path:'beers',
  loadChildren: () => import('cave-feature').then(item => item.caveRoutes),
  data: {preload: true}
}];
